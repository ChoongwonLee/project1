package com.project1.service;

import java.util.List;

import com.project1.dao.ReimDaoImpl;
import com.project1.model.Reimbursement;

public class ReimService {
	
	private ReimDaoImpl reimDao;
	
	public ReimService() {	
	}
	
	public ReimService(ReimDaoImpl reimDao) {
		super();
		this.reimDao = reimDao;
	}
	
	public Reimbursement requestReimbursement(double reimAmount, String submitted, String resolved, String description,
			int authorId, int resoleverId, int statusId, int reimTypeId) {
		
		reimDao.createReimbursement(reimAmount, submitted, resolved, description, authorId, resoleverId, statusId, reimTypeId);
		Reimbursement reim = reimDao.getRecentlyCreatedReim();
		if (reim == null) {
			throw new NullPointerException();
		}
		return reim;
	}
	
	public List<Reimbursement> findReimbursementsByAuthor(int authorId) {
		List<Reimbursement> reimList = reimDao.getReimbursementsByAuthorId(authorId);
		if (reimList == null || reimList.size() == 0) {
			return null;
		}
		return reimList;
	}
	
	public List<Reimbursement> findAllReimbursement() {
		List<Reimbursement> reimList = reimDao.getAllReimbursement();
		if (reimList == null || reimList.size() == 0) {
			return null;
		}
		return reimList;
	}
	
	public List<Reimbursement> filterReimbursementByStatus(int statusId) {
		List<Reimbursement> filteredList = reimDao.getReimbursementByStatus(statusId);
		if (filteredList == null || filteredList.size() == 0) {
			return null;
		}
		return filteredList;
	}
	
	public Reimbursement ApproveReimbursement(int reimId, int approvalCode, int resoleverId, String resolved) {
		reimDao.changeReimbursementStatus(reimId, approvalCode, resoleverId, resolved);
		Reimbursement reim = reimDao.getReimbursementByReimId(reimId);
		if (reim == null) {
			return null;
		}
		return reim;
	}
}
