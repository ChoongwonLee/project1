window.onload = function () {
    console.log('updated.js');
    getUpdatedReimbursements();
};


async function getUpdatedReimbursements() {
    try {
        let response = await fetch("http://localhost:8080/Project1/getallreimbursements.json", { method: "GET" });
        let reimb = await response.json();
        console.log(reimb);
        const dataBody = document.querySelector("#dataTable > tbody");
        reimb.forEach(obj => {
            const tr = document.createElement("tr");
            for (const key in obj) {
                const td = document.createElement("td");
                if (key == "reimAmount") {
                    obj[key] = (obj[key]).toLocaleString('en-US', {
                        style: 'currency',
                        currency: 'USD',
                      });
                }
                if (key == "statusId") {
                    if (obj[key] == 1) {
                        obj[key] = "PENDING";
                    } else if (obj[key] == 2) {
                        obj[key] = "APPROVED";
                    } else if (obj[key] == 3) {
                        obj[key] = "DENIED";
                    }
                }
                if (key == "reimTypeId") {
                    if (obj[key] == 1) {
                        obj[key] = "LODGING";
                    } else if (obj[key] == 2) {
                        obj[key] = "TRAVEL";
                    } else if (obj[key] == 3) {
                        obj[key] = "FOOD";
                    } else if (obj[key] == 4) {
                        obj[key] = "OTHER";
                    }
                }
                td.textContent = obj[key];
                tr.appendChild(td);
            }
            dataBody.appendChild(tr);
        });
    } catch (error) {
        console.error(error);
    }
};