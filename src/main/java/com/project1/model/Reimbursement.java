package com.project1.model;

public class Reimbursement {

	private int reimId;
	private double reimAmount;
	private String submitted;
	private String resolved;
	private String description;
	private int authorId;
	private int resoleverId;
	private int statusId;
	private int reimTypeId;
	
	public Reimbursement() {
	}
	
	public Reimbursement(double reimAmount, String submitted, String resolved, String description, 
			int authorId, int resoleverId, int statusId, int reimTypeId) {
		super();
		this.reimAmount = reimAmount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.authorId = authorId;
		this.resoleverId = resoleverId;
		this.statusId = statusId;
		this.reimTypeId = reimTypeId;
	}

	public Reimbursement(int reimId, double reimAmount, String submitted, String resolved, String description,
			int authorId, int resoleverId, int statusId, int reimTypeId) {
		super();
		this.reimId = reimId;
		this.reimAmount = reimAmount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.authorId = authorId;
		this.resoleverId = resoleverId;
		this.statusId = statusId;
		this.reimTypeId = reimTypeId;
	}

	public int getReimId() {
		return reimId;
	}

	public void setReimId(int reimId) {
		this.reimId = reimId;
	}

	public double getReimAmount() {
		return reimAmount;
	}

	public void setReimAmount(double reimAmount) {
		this.reimAmount = reimAmount;
	}

	public String getSubmitted() {
		return submitted;
	}

	public void setSubmitted(String submitted) {
		this.submitted = submitted;
	}

	public String getResolved() {
		return resolved;
	}

	public void setResolved(String resolved) {
		this.resolved = resolved;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getAuthorId() {
		return authorId;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}

	public int getResoleverId() {
		return resoleverId;
	}

	public void setResoleverId(int resoleverId) {
		this.resoleverId = resoleverId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public int getReimTypeId() {
		return reimTypeId;
	}

	public void setReimTypeId(int reimTypeId) {
		this.reimTypeId = reimTypeId;
	}

	@Override
	public String toString() {
		return "[reimId=" + reimId + ", reimAmount=" + reimAmount + ", submitted=" + submitted
				+ ", resolved=" + resolved + ", description=" + description + ", authorId="
				+ authorId + ", resoleverId=" + resoleverId + ", statusId=" + statusId 
				+ ", reimTypeId=" + reimTypeId + "]\n";
	}
	
}
