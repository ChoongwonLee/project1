# Employee Reimbursment System (ERS)

## Project Description
A reimbursement system for employees of a company. Employees can request expense reimbursement, and a financial manager is able to approve or deny the requests.

## Technologies Used
* Apache Tomcat Server
* PostgreSQL
* Java Servlet
* JDBC
* Jackson Databind
* JUnit and Mockito
* Log4J
* JavaScript

## Features
* Submit a reimbursement request
* Approve / Deny a reimbursement request
* Filter reimbursement status

## Getting Started
1. Be sure to have Apache Tomcat 9.0 installed.
1. Be sure to have the Java 8 runtime environment installed.
1. Run the following command to clone the project.
`git clone https://gitlab.com/ChoongwonLee/project1.git`
