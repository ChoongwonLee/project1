package com.project1.service;

import java.util.List;

import com.project1.dao.UserDaoImpl;
import com.project1.model.User;

public class UserService {

	private UserDaoImpl userDao;
	
	public UserService() {
	}

	public UserService(UserDaoImpl userDao) {
		super();
		this.userDao = userDao;
	}
	
	public User registerNewUser(String username, String password, String firstName, String lastName, String email,
			int userRole) {
		User existingUser = findUserByUsername(username);
		if (existingUser != null) {
			return null;
		}
		
		if (userRole > 2 && userRole < 1) {
			return null;
		}
		userDao.registerUser(username, password, firstName, lastName, email, userRole);
		
		User userCreated = findUserByUsername(username);
		
		return userCreated;
	}
	
	public User findSavedUser(String name, String password) {
		User user = userDao.getSavedUser(name, password);
		if (user.getUsername() != null) {
			return user;
		} else {
			return null;
		}
	}
	
	public User findUserByUsername(String username) {
		User user = userDao.getUserByUsername(username);
		if (user == null) {
			return null;
		} 
		return user;
	}
	
	public User findAuthorById(int id) {
		User user = userDao.getUserById(id);
		if (user.getUsername() != null) {
			return user;
		} else {
			return null;
		}
	}
	
	public List<User> findAllUsers() {
		List<User> userList = userDao.getAllUsers();
		if (userList.size() > 0) {
			return userList;
		} else {
			return null;
		}
	}
}
