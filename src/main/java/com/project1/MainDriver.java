package com.project1;

import java.util.List;

import com.project1.dao.ReimDaoImpl;
import com.project1.model.Reimbursement;
import com.project1.service.ReimService;

public class MainDriver {

	public static void main(String[] args) {
//		UserDaoImpl userDao = new UserDaoImpl();
//		UserService userService = new UserService(userDao);
//		User user = userService.registerNewUser("test7", "7777", "Joanna", "Anderson", "joanaanderson#email.com", 1);
//		System.out.println(user);
		ReimDaoImpl reimDao = new ReimDaoImpl();
		ReimService reimService = new ReimService(reimDao);
		
		List<Reimbursement> list = reimService.findReimbursementsByAuthor(1);
		System.out.println(list);
	}

}
