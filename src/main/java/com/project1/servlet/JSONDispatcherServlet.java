package com.project1.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Level;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project1.controller.ReimController;
import com.project1.controller.UserController;
import com.project1.model.User;

public class JSONDispatcherServlet {
	
	public static void process(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		MasterServlet.log.setLevel(Level.TRACE);
		switch (req.getRequestURI()) {
			case "/Project1/getsessionuser.json":
				MasterServlet.log.trace("User Session Request & Response");
				UserController.getSessionUser(req, res);
				break;
			case "/Project1/getreimbursements.json":
				MasterServlet.log.trace("User's Existing Reimbursements Request & Response");
				ReimController.getReimbursements(req, res);
				break;
			case "/Project1/getallreimbursements.json":
				MasterServlet.log.trace("Get All Reimbursements Request & Response");
				ReimController.getAllReimbursements(req, res);
				break;
			case "/Project1/getfilteredreimbursements.json":
				MasterServlet.log.trace("Get Filtered Reimbursements Request & Response");
				ReimController.filterReimbursement(req, res);
				break;
			case "/Project1/getupdatedreimbursements.json":
				MasterServlet.log.trace("Get Updated Reimbursements Request & Response");
				ReimController.getUpdatedReimburse(req, res);
				break;
			default:
				res.getWriter().write(new ObjectMapper().writeValueAsString(new User()));
		}
	}
}
