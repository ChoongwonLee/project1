package com.project1.dao;

import java.time.LocalDate;
import java.util.List;

import com.project1.model.Reimbursement;

public interface ReimbursementDao {

	void createReimbursement(double reimAmount, String submitted, String resolved, String description,
			int authorId, int resoleverId, int statusId, int reimTypeId);
	
	Reimbursement getRecentlyCreatedReim();
	
	List<Reimbursement> getReimbursementsByAuthorId(int authorId);

	List<Reimbursement> getAllReimbursement();
	
	List<Reimbursement> getReimbursementByStatus(int statusId);
	
	void changeReimbursementStatus(int reimId, int approvalCode, int resoleverId, String resolved);
	
	Reimbursement getReimbursementByReimId(int reimId);
}
