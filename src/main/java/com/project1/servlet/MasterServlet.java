package com.project1.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


public class MasterServlet extends HttpServlet {
	public static final Logger log = Logger.getLogger(MasterServlet.class);
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // for http1.1
		res.setHeader("Pragma", "no-cache"); // for http1.0
		res.setHeader("Expires", "0"); // for proxy
		req.getRequestDispatcher(RequestDispatcher.process(req)).forward(req, res);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // for http1.1
		res.setHeader("Pragma", "no-cache"); // for http1.0
		res.setHeader("Expires", "0"); // for proxy
		req.getRequestDispatcher(RequestDispatcher.process(req)).forward(req, res);
	}
}
