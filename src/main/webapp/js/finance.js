window.onload = function () {
    console.log('finance.js');
    getSessUser();
    getAllReimbursements();
};

async function getSessUser() {
    try {
        let response = await fetch("http://localhost:8080/Project1/getsessionuser.json", { method: "GET" });
        let user = await response.json();
        console.log(user);
        document.getElementById("welcomeHeader").innerText = `Current User: ${user.firstName} ${user.lastName}`;
    } catch (error) {
        console.error(error);
    }
};

async function getAllReimbursements() {
    try {
        let response = await fetch("http://localhost:8080/Project1/getallreimbursements.json", { method: "GET" });
        let reimb = await response.json();
        console.log(reimb);
        const dataBody = document.querySelector("#dataTable > tbody");
        reimb.forEach(obj => {
            const tr = document.createElement("tr");
            for (const key in obj) {
                const td = document.createElement("td");
                if (key == "reimAmount") {
                    obj[key] = (obj[key]).toLocaleString('en-US', {
                        style: 'currency',
                        currency: 'USD',
                      });
                }
                if (key == "statusId") {
                    if (obj[key] == 1) {
                        obj[key] = "PENDING";
                    } else if (obj[key] == 2) {
                        obj[key] = "APPROVED";
                    } else if (obj[key] == 3) {
                        obj[key] = "DENIED";
                    }
                }
                if (key == "reimTypeId") {
                    if (obj[key] == 1) {
                        obj[key] = "LODGING";
                    } else if (obj[key] == 2) {
                        obj[key] = "TRAVEL";
                    } else if (obj[key] == 3) {
                        obj[key] = "FOOD";
                    } else if (obj[key] == 4) {
                        obj[key] = "OTHER";
                    }
                }
                td.textContent = obj[key];
                tr.appendChild(td);
            }
            dataBody.appendChild(tr);
        });
    } catch (error) {
        console.error(error);
    }
};

const reimForm = document.getElementById("reimForm");
reimForm.addEventListener("submit", async function (e) {
    try {
        e.preventDefault();
        let selected = reimForm.statusId.value;
        let response = await fetch("http://localhost:8080/Project1/getallreimbursements.json", { method: "GET" });
        let reimb = await response.json();
        let filteredReimb = reimb;
        if (selected > 0) {
            filteredReimb = reimb.filter(obj => obj.statusId == selected);
             console.log(filteredReimb);
        }
        document.querySelector("tbody").remove();
        let dataBody = document.createElement("tbody");
        document.getElementById("dataTable").appendChild(dataBody);
        dataBody = document.querySelector("#dataTable > tbody");
        filteredReimb.forEach(obj => {
            const tr = document.createElement("tr");
            for (const key in obj) {
                const td = document.createElement("td");
                if (key == "reimAmount") {
                    obj[key] = (obj[key]).toLocaleString('en-US', {
                        style: 'currency',
                        currency: 'USD',
                      });
                }
                if (key == "statusId") {
                    if (obj[key] == 1) {
                        obj[key] = "PENDING";
                    } else if (obj[key] == 2) {
                        obj[key] = "APPROVED";
                    } else if (obj[key] == 3) {
                        obj[key] = "DENIED";
                    }
                }
                if (key == "reimTypeId") {
                    if (obj[key] == 1) {
                        obj[key] = "LODGING";
                    } else if (obj[key] == 2) {
                        obj[key] = "TRAVEL";
                    } else if (obj[key] == 3) {
                        obj[key] = "FOOD";
                    } else if (obj[key] == 4) {
                        obj[key] = "OTHER";
                    }
                }
                td.textContent = obj[key];
                tr.appendChild(td);
            }
            dataBody.appendChild(tr);
        }); 
    } catch (error) {
        console.error(error);
    }
});

// document.getElementById("singout").addEventListener("click", async function (e) {
//     try {
//         console.log("signout");
//         await fetch("http://localhost:8080/Project1/logoutrequest.change", { method: "POST"});
        
//         // window.location.assign("http://localhost:8080/Project1/html/index.html");
//         // window.history.forward("http://localhost:8080/Project1/html/index.html");
       
//         // document.cookie = 'JSESSIONID=; Max-Age=0; path=/; domain=' + location.host;
//         // // window.open('','_self').close();
//         // window.close();
//         // self.close();
//     } catch (error) {
//         console.log(error);
//     }
// });
