package com.project1.eval;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project1.dao.ReimDaoImpl;
import com.project1.model.Reimbursement;
import com.project1.service.ReimService;

public class ReimServiceTest {
	
	@Mock
	private ReimDaoImpl fakeReimDao;
	private ReimService reimService;
	private Reimbursement reim;
	private Reimbursement reimApproved;
	private List<Reimbursement> reimList = new ArrayList<>();
	
	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		reimService = new ReimService(fakeReimDao);
		reim = new Reimbursement(550, "2021-01-01", "2021-01-02", "Hotel for business trip", 3, 2, 1, 1);
		reimApproved = new Reimbursement(550, "2021-01-01", "2021-01-02", "Hotel for business trip", 3, 2, 2, 1);
		reimList.add(reim);
		reimList.add(reimApproved);
		doNothing().when(fakeReimDao).createReimbursement(550, "2021-01-01", "2021-01-02", "Hotel for business trip", 3, 2, 1, 1);
		when(fakeReimDao.getRecentlyCreatedReim()).thenReturn(reim);
		doNothing().when(fakeReimDao).changeReimbursementStatus(0, 2, 1, "2021-01-02");
		when(fakeReimDao.getReimbursementByReimId(0)).thenReturn(reimApproved);
		when(fakeReimDao.getReimbursementsByAuthorId(3)).thenReturn(reimList);
	}
	
	@Test
	public void requestReimbursementSuccess() {
		assertEquals(reim, reimService.requestReimbursement(550, "2021-01-01", "2021-01-02", "Hotel for business trip", 3, 2, 1, 1));
	}
	
	@Test
	public void approveReimbusemenstSuccess() {
		assertEquals(reimApproved, reimService.ApproveReimbursement(0, 2, 1, "2021-01-02"));
	}
	
	@Test
	public void findReimbusementsByAuthorSuccess() {
		assertEquals(reimList, reimService.findReimbursementsByAuthor(3));
	}
}
