package com.project1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	private final String URL = "jdbc:postgresql://rev-can-training.cmjhzbkaj9vv.us-east-2.rds.amazonaws.com:5432/ersdb";
	private final String username = "ersuser";
	private final String password = "Passw0rd";

	public Connection getDBConnection() throws SQLException {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return DriverManager.getConnection(URL, username, password);
	}
}