package com.project1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.project1.model.Reimbursement;

public class ReimDaoImpl implements ReimbursementDao {
	
	private DBConnection dbConn;
	
	public ReimDaoImpl() {
		dbConn = new DBConnection();
	}

	@Override
	public void createReimbursement(double reimAmount, String submitted, String resolved, String description,
			int authorId, int resoleverId, int statusId, int reimTypeId) {
		try (Connection conn = dbConn.getDBConnection()) {
			String sql = "insert into reimbursement(reimb_amount, reimb_submimitted, reimb_resolved, reimb_description, "
						 + "reimb_author, reimb_resolver, reimb_status_id, reimb_type_id) values(?, ?, ?, ?, ?, ?, ?, ?)";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setDouble(1, reimAmount);
			ps.setString(2, submitted);
			ps.setString(3, resolved);
			ps.setString(4, description);
			ps.setInt(5, authorId);
			ps.setInt(6, resoleverId);
			ps.setInt(7, statusId);
			ps.setInt(8, reimTypeId);
			ps.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public Reimbursement getRecentlyCreatedReim() {
		try (Connection conn = dbConn.getDBConnection()) {
			String sql = "select * from reimbursement where reimb_id = (select max(reimb_id) from reimbursement)";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			Reimbursement reim = new Reimbursement();
			while (rs.next()) {
				reim = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3),
						rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), 
						rs.getInt(9));
			}
			
			return reim;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Reimbursement> getReimbursementsByAuthorId(int authorId) {
		try (Connection conn = dbConn.getDBConnection()) {
			String sql = "select * from reimbursement where reimb_author = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, authorId);
			ResultSet rs = ps.executeQuery();
			
			List<Reimbursement> reimList = new ArrayList<>();
			while (rs.next()) {
				reimList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3),
						rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), 
						rs.getInt(9)));
			}
			
			return reimList;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Reimbursement> getAllReimbursement() {
		try (Connection conn = dbConn.getDBConnection()) {
			String sql = "select * from reimbursement";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			List<Reimbursement> reimList = new ArrayList<>();
			while (rs.next()) {
				reimList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3),
						rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), 
						rs.getInt(9)));
			}
			
			return reimList;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Reimbursement> getReimbursementByStatus(int statusId) {
		try (Connection conn = dbConn.getDBConnection()) {
			String sql = "select * from reimbursement where reimb_status_id = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, statusId);
			ResultSet rs = ps.executeQuery();
			
			List<Reimbursement> reimList = new ArrayList<>();
			while (rs.next()) {
				reimList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3),
						rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), 
						rs.getInt(9)));
			}
			
			return reimList;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void changeReimbursementStatus(int reimId, int approvalCode, int resoleverId, String resolved) {
		if (approvalCode > 3 && approvalCode < 2) {
			return;
		}
		try (Connection conn = dbConn.getDBConnection()) {
			String sql = "update reimbursement set reimb_status_id = ?, reimb_resolver = ?, reimb_resolved = ? where reimb_status_id = ? and reimb_id = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, approvalCode);
			ps.setInt(2, resoleverId);
			ps.setString(3, resolved);
			ps.setInt(4, 1);
			ps.setInt(5, reimId);
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Reimbursement getReimbursementByReimId(int reimId) {
		try (Connection conn = dbConn.getDBConnection()) {
			String sql = "select * from reimbursement where reimb_id = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reimId);
			ResultSet rs = ps.executeQuery();
			
			Reimbursement reim = new Reimbursement();
			while (rs.next()) {
				reim = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3),
						rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), 
						rs.getInt(9));
			}
			
			return reim;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
