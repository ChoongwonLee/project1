window.onload = function () {
    console.log('filtered.js');
    getFilteredReimbursements();
};


async function getFilteredReimbursements() {
    try {
        let response = await fetch("http://localhost:8080/Project1/getfilteredreimbursements.json", { method: "GET" });
        let reimb = await response.json();
        console.log(reimb);
        const dataBody = document.querySelector("#dataTable > tbody");
        reimb.forEach(obj => {
            const tr = document.createElement("tr");
            for (const key in obj) {
                const td = document.createElement("td");
                td.textContent = obj[key];
                tr.appendChild(td);
            }
            dataBody.appendChild(tr);
        });
        const confirm = document.getElementById("confirm");
        if (reimb[0].statusId !== 1) {
            confirm.parentNode.removeChild(confirm);
        }
    } catch (error) {
        console.error(error);
    }
};