package com.project1.servlet;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;

import com.project1.controller.ReimController;
import com.project1.controller.UserController;

public class RequestDispatcher {

	public static String process(HttpServletRequest req) {
		MasterServlet.log.setLevel(Level.TRACE);
		switch (req.getRequestURI()) {
		case "/Project1/login.change":
			MasterServlet.log.trace("Login Request");
			return UserController.login(req);
		case "/Project1/reimburserequest.change":
			MasterServlet.log.trace("Reimbursement Creation Request");
			return ReimController.receiveReimbursement(req);
		case "/Project1/filterrequest.change":
			MasterServlet.log.trace("Filter Reimbursements Request");
			return ReimController.receiveFilterRequest(req);
		case "/Project1/updaterequest.change":
			MasterServlet.log.trace("Update Reimbursements Request");
			return ReimController.receiveUpdateRequest(req);
		case "/Project1/logoutrequest.change":
			MasterServlet.log.trace("Logout Request");
			return UserController.logout(req);
		default:
			return "html/unsuccessfullogin.html";
		}
	}
}
