package com.project1.dao;

import java.util.List;

import com.project1.model.User;

public interface UserDao {
	
	void registerUser(String username, String password, String firstName, String lastName, String email, int userRole);
	
	User getSavedUser(String username, String password);
	
	User getUserByUsername(String username);
	
	User getUserById(int id);
	
	List<User> getAllUsers();
}
