package com.project1.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project1.dao.UserDaoImpl;
import com.project1.model.User;
import com.project1.service.UserService;

public class UserController {
	
	private static  UserDaoImpl userDao = new UserDaoImpl();
	private static UserService userService = new UserService(userDao);
	
	public static String login(HttpServletRequest req) {
		try {
			User user = (User) req.getSession().getAttribute("currentUser");
			if (user != null) {
				req.getSession().removeAttribute("currentUser");
			}
			System.out.println("in user controller login");
			if (!req.getMethod().equals("POST")) {
				return "html/index.html";
			}
			
			user = userService.findSavedUser(req.getParameter("username"), req.getParameter("password"));

			if (user == null) {
				return "wrongcreds.change";
			} else {
				req.getSession().setAttribute("currentUser", user);
				
				if (user.getUserRole() == 2) {
					return "html/finance.html";
				}
				return "html/start.html";
			}
		} catch (NullPointerException e) {
			return "wrongcreds.change";
		}
		
	}
	
	public static void getSessionUser(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException{
		User user = (User) req.getSession().getAttribute("currentUser");
		res.getWriter().write((new ObjectMapper().writeValueAsString(user)));
	}

	public static String logout(HttpServletRequest req) {
		User user = (User) req.getSession().getAttribute("currentUser");
		req.getSession().removeAttribute("currentUser");
		
		return "html/index.html";
	}
}
