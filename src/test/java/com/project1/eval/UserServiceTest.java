package com.project1.eval;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project1.dao.UserDaoImpl;
import com.project1.model.User;
import com.project1.service.UserService;


public class UserServiceTest {

	@Mock
	private UserDaoImpl fakeUserDao;
	private UserService userService;
	private User user;
	
	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		userService = new UserService(fakeUserDao);
		user = new User("TestUser", "1111", "Tester", "Testist", "test@email.com", 1);
		when(fakeUserDao.getUserByUsername("TestUser")).thenReturn(user);
		doNothing().when(fakeUserDao).registerUser("TestUser", "1111", "Tester", "Testist", "test@email.com", 1);
		when(fakeUserDao.getSavedUser("TestUser", "1111")).thenReturn(user);;
		when(fakeUserDao.getUserById(0)).thenReturn(user);
	}
	
	@Test
	public void findDuplicatedWhenRegisterSuccess() {
		assertEquals(null, userService.registerNewUser("TestUser", "1111", "Tester", "Testist", "test@email.com", 1));
	}
	
	@Test
	public void findSavedUserSuccess() {
		assertEquals(user, userService.findSavedUser("TestUser", "1111"));
	}
	
	@Test
	public void findUserByUsernameSuccess() {
		assertEquals(user, userService.findUserByUsername("TestUser"));
	}
	
	@Test
	public void findUserByAuthorIdSuccess() {
		assertEquals(user, userService.findAuthorById(0));
	}
	
	
}
