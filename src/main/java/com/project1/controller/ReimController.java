package com.project1.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.project1.dao.ReimDaoImpl;
import com.project1.model.Reimbursement;
import com.project1.model.User;
import com.project1.service.ReimService;

public class ReimController {

	private static ReimDaoImpl reimDao = new ReimDaoImpl();
	private static ReimService reimService = new ReimService(reimDao);
	
	public static void getReimbursements(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		User user = (User) req.getSession().getAttribute("currentUser");
		int userId = user.getUserId();
		List<Reimbursement> reimList = reimService.findReimbursementsByAuthor(userId);

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		res.setStatus(200);
		res.setContentType("application/json");
		res.getWriter().write(mapper.writeValueAsString(reimList));
	}
	
	public static String receiveReimbursement(HttpServletRequest req) {
		User user = (User) req.getSession().getAttribute("currentUser");
		
		double amount = Double.parseDouble(req.getParameter("amount"));
		int typeId = Integer.parseInt(req.getParameter("reimTypeId"));
		String date = LocalDate.now().toString();
		int userId = user.getUserId();
		reimService.requestReimbursement(amount, date, null, 
				req.getParameter("description"), user.getUserId(), userId, 1, typeId);
		return "html/afterreimbreq.html";
	}
	
	public static void getAllReimbursements(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		List<Reimbursement> reimList = reimService.findAllReimbursement();
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		res.setStatus(200);
		res.setContentType("application/json");
		res.getWriter().write(mapper.writeValueAsString(reimList));
	}
	
	public static String receiveFilterRequest(HttpServletRequest req) {
		req.getSession().setAttribute("filterStatus", req.getParameter("statusId"));;
		return "html/filtered.html";
	}
	
	public static void filterReimbursement(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		User user = (User) req.getSession().getAttribute("currentUser");
		int userId = user.getUserId();
		String date = LocalDate.now().toString();
		int statusId = Integer.parseInt((String) req.getSession().getAttribute("filterStatus"));
		List<Reimbursement> reimList = reimService.filterReimbursementByStatus(statusId);
		ObjectMapper mapper = new ObjectMapper();
		res.setStatus(200);
		res.setContentType("application/json");
		res.getWriter().write(mapper.writeValueAsString(reimList));
	}
	
	public static String receiveUpdateRequest(HttpServletRequest req) {
		User user = (User) req.getSession().getAttribute("currentUser");
		int userId = user.getUserId();
		int reimId = Integer.parseInt((String) req.getParameter("reimId"));
		int statusId = Integer.parseInt((String) req.getParameter("statusMade"));
		String date = LocalDate.now().toString();
		Reimbursement reim = reimService.ApproveReimbursement(reimId, statusId, userId, date);
		req.getSession().setAttribute("updatedReim", reim);
		return "html/updated.html";
	}
	
	public static void getUpdatedReimburse(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		List<Reimbursement> reimList = reimService.findAllReimbursement();
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		res.setStatus(200);
		res.setContentType("application/json");
		res.getWriter().write(mapper.writeValueAsString(reimList));
	}
	
}
